use std::path::PathBuf;

use anyhow::Context;
use chrono::{DateTime, NaiveDate, Offset, Utc};
use geo::Point;
use gpx::{Gpx, Track, TrackSegment, Waypoint};
use structopt::StructOpt;

#[derive(StructOpt)]
pub struct Opt {
    #[structopt()]
    input: PathBuf,
    #[structopt(short, long)]
    output: PathBuf
}

fn main() -> anyhow::Result<()> {
    let opt = Opt::from_args();

    let nmea_lines = std::fs::read(&opt.input)
        .context(format!("Failed to read {:?}", opt.input))?;

    let mut nmea_parser = nmea0183::Parser::new();

    let waypoints = nmea_parser.parse_from_bytes(&nmea_lines)
        // Filter errors
        .filter_map(|entry| {
            match entry {
                Ok(message) => Some(message),
                Err(e) => {
                    println!("NMEA error: {:?}, ignoring row", e);
                    None
                }
            }
        })
        // Filter GGA
        .filter_map(|entry| {
            match entry {
                nmea0183::ParseResult::RMC(Some(rmc)) => {
                    Some(rmc)
                }
                _ => None
            }
        })
        .map(|rmc| {
            let mut wpt = Waypoint::new(Point::new(rmc.longitude.as_f64(), rmc.latitude.as_f64()));
            let nmea_date = rmc.datetime.date;
            let nmea_time = rmc.datetime.time;
            let date = NaiveDate::from_ymd(nmea_date.year as i32, nmea_date.month as u32, nmea_date.day as u32)
                .and_hms_micro(
                    nmea_time.hours as u32,
                    nmea_time.minutes as u32,
                    nmea_time.seconds.floor() as u32,
                    (nmea_time.seconds.fract() * 1_000_000.) as u32
                );
            // let time = NaiveDateTime::gga.time.
            wpt.time = Some(DateTime::from_utc(date, Utc));

            wpt
        })
        .collect::<Vec<_>>();

    let track = Track {
        segments: vec![TrackSegment {
            points: waypoints
        }],
        .. Default::default()
    };

    let gpx = Gpx {
        version: gpx::GpxVersion::Gpx11,
        tracks: vec![track],
        .. Default::default()
    };

    let outfile = std::fs::File::create(&opt.output)
        .context(format!("Failed to open {:?}", opt.output))?;
    gpx::write(&gpx, outfile)
        // NOTE: the result here does not impl Sync, so we can't anyhow it
        .expect("Failed to write gpx file");

    Ok(())
}
